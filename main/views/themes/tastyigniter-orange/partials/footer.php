</div>
</div>
<footer id="page-footer">
    <?php echo get_partial('content_footer'); ?>

    <div class="main-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-6 col-sm-3 col-sm-offset-6 ">
                    <div class="footer-links">
                        <i>Product Of </i>
                        <img class="" src="<?=base_url('assets/images/proposed.png')?>" height="60px">
                    </div>
                </div>
                <div class="col-xs-6 col-sm-3 ">
                    <div class="footer-links">
                        <i>Powered By</i>
                        <img src="<?=base_url('assets/images/cisco.png')?>" height="40px">
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<?php $custom_script = get_theme_options('custom_script'); ?>
<?php if (!empty($custom_script['footer'])) {
    echo '<script type="text/javascript">' . $custom_script['footer'] . '</script>';
}; ?>
</body>
</html>